// normal function
function sum(a, b) {
    return a + b;
}

// anonymous function
var addOper = function (a, b) {
    return a + b;
}

// arrow function
var addOperArrow = (a, b) => {a + b};

//prints function
console.log(addOperArrow);

//prints addition value
console.log(addOperArrow(5, 6));

//prints addition value
var res = addOperArrow(5, 6);
console.log(res);

//multiline statement - u need to specify the return statement
var addOperArrow1 = (a, b, c, d) => {
    var sum1 = a + b;
    var sum2 = c + d;
    var result = sum1 - sum2;
    return result;
}
