function swap(a, b) {
    var c = a;
    a = b;
    b = c;
}

var exchange = function (a, b) {
    console.log("Before swap, a="+a+", b="+b);
    var c = a;
    a = b;
    b = c;
    console.log("After swap, a="+a+", b="+b);
}
