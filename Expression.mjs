import { msg as Message, sum as addOper, sub, mul as mulOper, div} from "./Arithmetic.mjs";
import x from "./Arithmetic.mjs";

function solveExpression(a, b, c, d, e, f) {
	//((a+b)-(c+d))*(e/f)
	var addRes1 = addOper(a, b);
	var addRes2 = addOper(c, d);
	var subRes = sub(addRes1, addRes2);
	var divRes = div(e, f);
	var mulRes = mulOper(subRes, divRes);
	x(Message, mulRes);
}

solveExpression(5, 6, 3, 2, 16, 4)