function div(a, b) {
    if (b == 0) {
        throw new Error("Invalid Input:: b should be greater than 0");
    }
    return a / b;
}

var result = div(15, 3);

console.log("result----->", result);

//const numerator = 100, denominator = 'a';
/*
// program to show try...catch in a program
console.log(numerator/denominator);

// forgot to define variable a      
console.log(a);
console.log("Completed");
*/

const numerator = 100, denominator = new String('a');

if (denominator == 0 || denominator instanceof String) {
    throw new Error("Denominator should be number and greater than 0");
}
console.log(numerator / denominator);
try {
    // forgot to define variable a      
    console.log(a);
}
catch (error) {
    console.log('An error caught');
    console.log('Error message: ' + error);
} finally {
    console.log("Inside finally block........");
}
console.log("Completed........");


//try without finally but with catch block. This will work
try {
    // forgot to define variable a      
    console.log(a1);
} catch (error) {
    console.log("Inside catch block........", error);
}
console.log("done");

//try without catch but with finally block. This will work
try {
    // forgot to define variable a      
    console.log(b1);
} finally {
    console.log("Inside finally block........");
}
console.log("done");