var ageVal = 10;

function swap(a, b) {
    console.log("Before swap, a="+a+", b="+b);
    var c = a;
    a = b;
    b = c;
    console.log("After swap, a="+a+", b="+b);
}

var obj1 = {
    name: "tiger",
    age: ageVal,
    fnVal: swap,
    fnVal1: function () {
        console.log("I'm Anonymous function......");
    }
}
