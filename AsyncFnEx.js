// Normal Function
function testAsync() {
    console.log();
    console.log("Testing Async.............");
}

let promise = new Promise(function (resolve, reject) {
    setTimeout(()=> {resolve(1)}, 5000)
});

// Async/Await Function
async function assemble() {
    testAsync();

    let result = await promise;
    //let result = promise;
    console.log("result----", result);
    console.log("Inside assemble function............");
}

assemble();