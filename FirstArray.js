function firstArray() {
    var arr = [10, 15, 20, 25];
    console.log("The value of arr[0]="+arr[0]);
    console.log("The value of arr[1]="+arr[1]);
    console.log("The value of arr[2]="+arr[2]);
    console.log("The value of arr[3]="+arr[3]);
}

function iterateFirstArray() {
    var arrVal = [15, 10, 8, 25, 17, 3];
    arrVal.sort();
    arrVal.reverse();
    for (var i=0; i<arrVal.length; i++) {
        console.log("The value of arrVal["+i+"]="+arrVal[i]);
    }
}

function storeInArray() {
    var arr = new Array();
    var k = 0;
    for (var i=1; i<=10; i++) {
        if (i %2 == 0) {
            arr[i] = i;
        }
    }
    printGivenArray(arr);
}

function printGivenArray(arr) {
    if (arr instanceof Array) {
        for (var i=0; i<arr.length; i++) {
            console.log("The value of arr["+i+"]="+arr[i]);
        }    
    } else {
        console.log("It is not array but variable :: "+arr);
    }
}
