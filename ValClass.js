class Val {
	
	constructor(a,b) {
		this.a = a;
		this.b = b;
	}
	
	getA() {
		return this.a;
	}
	
    setA(a) {
        this.a = a;
    }

	getB() {
		return this.b;
	}

    setB(b) {
        this.b = b;
    }
}

function swapTwoNumbers(x,y) {
	var c = x;
	var x = y;
	var y = c;
	
	var valObj = new Val(x, y);
	return valObj;
}

function printVal(obj) {
    if (obj instanceof Val) {
        console.log(obj.getA()+', '+ obj.b);
    }
}

var obj = swapTwoNumbers(5,7);
printVal(obj)
