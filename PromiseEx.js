var successFailFn = (resolve, reject) => {
    //api call
    const chkVal = false;
    if (chkVal) {
        resolve("Promise is successful........");
    } else {
        reject("Promise is failed.......");
    }
}

const promiseObj = new Promise(successFailFn);

promiseObj
.then(function sample() {
    console.log("Inside sample");
}).then(function sample1() {
    console.log("Inside sample1");
}).then(function sample2() {
    console.log("Inside sample2");
}).catch(function errFn() {
    console.log("Error occured. Inside errFn")
}).then(function sample3() {
    console.log("success, but sample3")
});

console.log(promiseObj);