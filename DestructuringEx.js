function deStruct() {

    var obj1 = {
        name: 'Tiger',
        age: 10,
        email: "t1@gmail.com",
        district: "kgi",
        addOper: (a, b) => a + b
    }

    var {name, email, district, addOper} = obj1;

    console.log(name);
    console.log(email);
    console.log(district);

    console.log(addOper);

    console.log("Add Operation---->", addOper(7,6));
}

deStruct();

var arr1 = [4,5,6,76,43];

var [x, y] = arr1;

console.log(x, y);

var arr2 = [15];

var [c, d] = arr2;

console.log(c, d);