
function mapEx1() {
    let mapObj1 = new Map();

    mapObj1.set("Tiger", 10);
    mapObj1.set("Tiger", 20);
    mapObj1.set("Lion", 10);
    mapObj1.set("Zebra", 5);
    mapObj1.set(null, 10);
    mapObj1.set(null, 17);

    var age = mapObj1.get("Zebra");
    console.log("age="+age);

    console.log("before delete=", mapObj1);

    mapObj1.delete("Zebra");

    console.log("after delete=", mapObj1);
    
    return mapObj1;
}

function getKeys(mapOb) {
    if (mapOb instanceof Map) {
        var keys = mapOb.keys();
        console.log("typeOf for keys=", typeof keys);
        if (keys instanceof Set) {
            console.log("Set obj");
        } else {
            console.log("Not Set obj");
        }
        console.log("Keys=", mapOb.keys());
    }
}

function getValues(mapOb) {
    if (mapOb instanceof Map) {
        console.log("Keys=", mapOb.values());
    }
}

function iterateMap(mapOb) {
    for (const [key, val] of mapOb) {
        console.log("key="+key+", value="+val);
    }
}

function iterateMapKeys(mapOb) {
    for (const key of mapOb.keys()) {
        console.log("Iterate the Map Keys ---> key="+key+", value="+mapOb.get(key));
    }
}

function clearMap(mapOb) {
    console.log("Before clear, set value=", mapOb);
    mapOb.clear();
    console.log("After clear, set value=", mapOb);
}

var mapObj2 = mapEx1();
iterateMap(mapObj2);
iterateMapKeys(mapObj2);
getKeys(mapObj2);
getValues(mapObj2);
clearMap(mapObj2);
//iterateSetObj(setObj2);
//clearSet(setObj2);