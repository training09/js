var empObj = {
    name: "tiger",
    age: 10
}

//get method
var getOper = {
    get: function (obj, prop) {
        return obj[prop];
    }
}

var setOper = {
    set: function (obj, prop, value) {
        obj[prop] = value;
        //return;
    }
}

var getProxyObj = new Proxy(empObj, getOper);
console.log("..................get proxy example..................");
console.log(getProxyObj.name);
console.log(getProxyObj.age);
console.log(getProxyObj.height);


var setProxyObj = new Proxy(empObj, setOper);
console.log("..................set proxy example..................");
console.log(setProxyObj.name);
console.log(setProxyObj.age);
setProxyObj.height='100CMS';
setProxyObj.addOper = (a, b) => a + b;
console.log(setProxyObj.height);

// console.log("..................get proxy example..................");
// console.log(getProxyObj.name);
// console.log(getProxyObj.age);
// console.log(getProxyObj.height);

var getProxyObj1 = new Proxy(empObj, getOper);
console.log("..................get proxy example1..................");
console.log(getProxyObj1.name);
console.log(getProxyObj1.age);
console.log(getProxyObj1.height);
var result = getProxyObj1.addOper(50,25);
console.log("result->", result);

console.log("..................Direct Object..................");
console.log("--------empObj------>", empObj);
