class Parent {
    
    constructor(name) {
        this.name = name;
        this.age = 10;
    }

    sample() {
        console.log("Inside Parent - Sample1")
    }

    test() {
        console.log("Inside Parent - Test")
    }
}

class Child extends Parent {

    constructor(name, email) {
        super(name);
        this.age = 20;
        this.email = email;
    }

    sample() {
        console.log("Inside Child - Sample")
    }

    print() {
        console.log("Inside Child - Print method")
    }
}


var childObj = new Child("Tiger", "t1@gmail.com");
console.log("---------------------------");
console.log(childObj.name);
console.log(childObj.age);
console.log(childObj.email);

childObj.sample();
childObj.test();
childObj.print();
console.log("---------------------------");
