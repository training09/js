export const msg = "Expression Result :: ";

export function sum(a,b) {
	return a + b;
}

export function sub(a,b) {
	var res;
	if (a > b) {
		res = a - b;
	} else {
		res = b - a;
	}
	//ternary operator
	res = (a>b) ? (a-b) : (b-a);
	
	return res;
}

export function mul(a,b) {
	return a * b;	
}

export function div(a,b) {
	return a / b;	
}