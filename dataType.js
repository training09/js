//declaration
var a1;
//initialization
a1 = 10;
//re-initialization
a1 = 20;
//declaration & initialization
var b1 = 30;

//function scoped
function chkDataType() {
	var i = 10;
	if (i == 10) {
		var a = 100;
		//let b = 110;
		//const c = 120;
		//console.log("The value of a="+a+", b="+b+", c="+c);
	}
	console.log("The value of a="+a);//+", b="+b+", c="+c);
}

//block scoped
function chkDataType1() {
	var i = 10;
	if (i == 10) {
		var a = 100;
		let b = 110;
		const c = 120;
		// This line will work
		console.log("The value of a="+a+", b="+b+", c="+c);
	}
	// This line won't work as let & const are block scoped
	console.log("The value of a="+a);//+", b="+b+", c="+c);
}

//re-declaration not allowed for let & const
function chkDataType2() {
	var a = 100;
	var a = 200;
	console.log("The value of a="+a);
	/*let b = 110;
	let b = 120;
	console.log("The value of b="+b);*/
	//const c = 120;
	//const c = 130;
	console.log("The value of c="+c);
}

//re-initialization not allowed for const
function chkDataType3() {
	var a = 100;
	a = 200;
	console.log("The value of a="+a);
	let b = 110;
	b = 120;
	console.log("The value of b="+b);
	const c = 120;
	c = 130;
	console.log("The value of c="+c);
}