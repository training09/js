function first(val) {
    console.log("Inside first method");
    val();
}

function second() {
    console.log("Inside second method");
}

first(second);
