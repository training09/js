import pr from "./Print.mjs";

class EmployeeClass {

    constructor(a, b) {
        this.name = a;
        this.age = b;
    }

    getAge() {
        return this.age;
    }

    getName() {
        return this.name;
    }
}

function exeClass() {
    var empObj = new EmployeeClass(5,6);
    pr("The value of name::", empObj.getName());
    pr("The value of age::", empObj.getAge());
}

exeClass();