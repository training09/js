
function setEx1() {
    const setObj1 = new Set();
    setObj1.add("Tiger");
    setObj1.add("Lion");
    setObj1.add("Tiger");
    setObj1.add("Zebra");
    setObj1.add("Lion");
    setObj1.add("Tiger");
    setObj1.add("Monkey");
    setObj1.add("Monkey");
    setObj1.add("Monkey");
    setObj1.add("Elephant");

    //console.log(setObj1);

    setObj1.delete("Monkey");

    console.log("keys =", setObj1.keys());

    console.log("values =", setObj1.values());

    //console.log("After Delete--->", setObj1);
    return setObj1;
}

function iterateSetObj(setOb) {
    for (const val of setOb) {
        console.log(val);
    }
}

function clearSet(setObj) {
    console.log("Before clear, set value=", setObj);
    setObj.clear();
    console.log("After clear, set value=", setObj);
}

var setObj2 = setEx1();
iterateSetObj(setObj2);
clearSet(setObj2);